package models

import "gorm.io/gorm"

type Img struct {
	gorm.Model
	GoalID int
	Url    string
}
