package models

import (
	"context"
	"gorm.io/gorm"
	"mime/multipart"
)

type Goal struct {
	gorm.Model
	ID          int    `gorm:"primaryKey;autoIncrement:true"`
	Author      string `json:"goalCreator"`
	AuthorAva   string `json:"imgAvatar"`
	AuthorId    int    `json:"goalCreatorId"`
	Title       string `json:"goalTitle"`
	Description string `json:"goalDescription"`
	UrlImgs     []Img  `json:"imgContent" gorm:"foreignKey:GoalID"`
	Date        string `json:"date"`
	Users     []User  `json:"followers" gorm:"many2many:user_goal;primaryKey"`
}

type GoalsUsecase interface {
	SaveGoal(ctx context.Context, session string, fileHandlers map[string][]*multipart.FileHeader, Goal *Goal) error
	GetGoals(ctx context.Context, session string) ([]Goal, error)
	GetUserGoals(c context.Context, session string, goalOwnerID int) ([]Goal, error)
	GetGoal(c context.Context, session string, goalID int) (*Goal, error)
}

type GoalsRepository interface {
	SaveGoal(ctx context.Context, Goal *Goal, userOwner *User, fileHandlers map[string][]*multipart.FileHeader) error
	GetGoals(ctx context.Context) ([]Goal, error)
	GetUserGoals(ctx context.Context, u *User) ([]Goal, error)
	GetGoal(ctx context.Context, id int) (*Goal, error)
}
