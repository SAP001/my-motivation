package usecase

import (
	"context"
	"mime/multipart"
	"my-motivation/internal/app/models"
	_sessionManager "my-motivation/internal/app/session/psql"
	"time"
)

type GoalUsecase struct {
	UserRepo       models.UserRepository
	GoalRepo       models.GoalsRepository
	contextTimeout time.Duration
	sessionManager *_sessionManager.SessionsManagerPsql
}

func NewGoalUsecase(ur models.UserRepository, pr models.GoalsRepository, timeout time.Duration, sm *_sessionManager.SessionsManagerPsql) models.GoalsUsecase {
	return &GoalUsecase{
		UserRepo:       ur,
		GoalRepo:       pr,
		contextTimeout: timeout,
		sessionManager: sm,
	}
}

func (pu *GoalUsecase) SaveGoal(c context.Context, session string, fileHandlers map[string][]*multipart.FileHeader, Goal *models.Goal) error {
	userId, err := pu.sessionManager.GetUserId(session)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(c, pu.contextTimeout)
	defer cancel()

	userOwner, err := pu.UserRepo.GetUserById(ctx, userId)
	if err != nil || userOwner == nil {
		return err
	}

	err = pu.GoalRepo.SaveGoal(ctx, Goal, userOwner, fileHandlers)
	if err != nil {
		return err
	}

	return nil
}

func (pu *GoalUsecase) GetGoals(c context.Context, session string) ([]models.Goal, error) {
	userId, err := pu.sessionManager.GetUserId(session)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(c, pu.contextTimeout)
	defer cancel()

	_, err = pu.UserRepo.GetUserById(ctx, userId)
	if err != nil {
		return nil, err
	}

	Goals, err := pu.GoalRepo.GetGoals(ctx)
	if err != nil {
		return nil, err
	}

	return Goals, nil
}

func (pu *GoalUsecase) GetUserGoals(c context.Context, session string, goalOwnerID int) ([]models.Goal, error) {
	userId, err := pu.sessionManager.GetUserId(session)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(c, pu.contextTimeout)
	defer cancel()

	_, err = pu.UserRepo.GetUserById(ctx, userId)
	if err != nil {
		return nil, err
	}

	goalOwner, err := pu.UserRepo.GetUserById(ctx, goalOwnerID)
	if err != nil {
		return nil, err
	}

	Goals, err := pu.GoalRepo.GetUserGoals(ctx, goalOwner)
	if err != nil {
		return nil, err
	}

	return Goals, nil
}

func (pu *GoalUsecase) GetGoal(c context.Context, session string, goalID int) (*models.Goal, error) {
	userId, err := pu.sessionManager.GetUserId(session)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(c, pu.contextTimeout)
	defer cancel()

	_, err = pu.UserRepo.GetUserById(ctx, userId)
	if err != nil {
		return nil, err
	}

	goal, err := pu.GoalRepo.GetGoal(ctx, goalID)
	if err != nil {
		return nil, err
	}

	author, err := pu.UserRepo.GetUserById(ctx, goal.AuthorId)
	if err != nil {
		return nil, err
	}
	goal.Author = author.FirstName + " " + author.LastName
	return goal, nil
}
