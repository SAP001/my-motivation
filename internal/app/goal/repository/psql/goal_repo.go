package psql

import (
	"context"
	"fmt"
	"gorm.io/gorm"
	"io"
	"mime/multipart"
	"my-motivation/internal/app/models"
	"my-motivation/internal/pkg/utils/hasher"
	"os"
	"time"
)

type GoalRepoPsql struct {
	DB *gorm.DB
}

func NewGoalRepoPsql(db *gorm.DB) *GoalRepoPsql {
	return &GoalRepoPsql{DB: db}
}

func (urp *GoalRepoPsql) SaveGoal(ctx context.Context, newGoal *models.Goal, userOwner *models.User, fileHandlers map[string][]*multipart.FileHeader) error {
	newGoal.AuthorId = userOwner.ID
	newGoal.Users = append(newGoal.Users, *userOwner)
	err := urp.DB.WithContext(ctx).Create(newGoal).Error
	//urp.DB.Session(&gorm.Session{FullSaveAssociations: true}).Updates(userOwner)
	if err != nil {
		return err
	}
	urp.storeImg(ctx, newGoal, fileHandlers)
	fmt.Println("Goal added")
	return nil
}

func (urp *GoalRepoPsql) storeImg(ctx context.Context, newGoal *models.Goal, fileHandlers map[string][]*multipart.FileHeader) error {

	for name, fileHeader := range fileHandlers {
		newImg := models.Img{}
		file, err := fileHeader[0].Open()
		if err != nil {
			return err
		}
		t := time.Now()
		salt := fmt.Sprintf(t.Format(time.RFC3339))
		genFileName := hasher.Hash(name + salt)
		defer file.Close()
		localImg, err := os.OpenFile("./static/goals/"+genFileName + ".png", os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			return err
		}
		newImg.Url = "/static/goals/" + genFileName + ".png"
		newImg.GoalID = newGoal.ID
		err = urp.DB.Create(&newImg).Error
		if err != nil {
			return err
		}
		defer localImg.Close()
		_, _ = io.Copy(localImg, file)
	}
	return nil
}

func (urp *GoalRepoPsql) GetGoals(ctx context.Context) ([]models.Goal, error) {
	var Goals []models.Goal
	err := urp.DB.WithContext(ctx).Preload("UrlImgs").Find(&Goals).Error
	if err != nil {
		return nil, err
	}
	for i, _ := range Goals {
		u := models.User{}
		err = urp.DB.WithContext(ctx).First(&u, "id = ?", Goals[i].AuthorId).Error
		if err != nil {
			return nil, err
		}
		Goals[i].Author = u.FirstName + " " + u.LastName
		Goals[i].AuthorAva = u.Avatar
	}
	return Goals, nil
}

func (urp *GoalRepoPsql) GetUserGoals(ctx context.Context, u *models.User) ([]models.Goal, error) {
	var Goals []models.Goal
	err := urp.DB.WithContext(ctx).Preload("UrlImgs").Where("author_id = ?", u.ID).Find(&Goals).Error
	if err != nil {
		return nil, err
	}
	return Goals, nil
}

func (urp *GoalRepoPsql) GetGoal(ctx context.Context, goalId int) (*models.Goal, error) {
	goal := &models.Goal{}
	goal.ID = goalId
	err := urp.DB.WithContext(ctx).Preload("UrlImgs").Preload("Users").First(goal).Error
	if err != nil {
		return nil, err
	}
	return goal, nil
}
