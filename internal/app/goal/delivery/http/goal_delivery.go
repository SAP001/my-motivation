package http

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"my-motivation/internal/app/models"
	"my-motivation/internal/pkg/utils/logger"
	"net/http"
	"strconv"
)

type GoalHandler struct {
	GoalUsecase models.GoalsUsecase
	logger logger.LoggerModel
}

func NewGoalHandler(r *mux.Router, pu models.GoalsUsecase, l logger.LoggerModel) {
	handler := &GoalHandler{
		GoalUsecase: pu,
		logger: l,
	}
	r.HandleFunc("/goals", handler.allGoals).Methods("GET", "OPTIONS")
	r.HandleFunc("/user/goals/{userID}", handler.userGoals).Methods("GET", "OPTIONS")
	r.HandleFunc("/goal/{goalID}", handler.getGoal).Methods("GET", "OPTIONS")
	r.HandleFunc("/goals/add", handler.addGoal).Methods("POST", "OPTIONS")
}

func (ph *GoalHandler) userGoals(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusForbidden)
		return
	}

	userId, err := strconv.Atoi(mux.Vars(r)["userID"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	Goals, err := ph.GoalUsecase.GetUserGoals(r.Context(), session.Value, userId)
	if err != nil {
		ph.logger.Error(err.Error())
		return
	}
	jsonValue, _ := json.Marshal(Goals)
	ph.logger.Debug("Goals sends")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonValue))
}

func (ph *GoalHandler) allGoals(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusForbidden)
		return
	}
	Goals, err := ph.GoalUsecase.GetGoals(r.Context(), session.Value)
	if err != nil {
		ph.logger.Error(err.Error())
		return
	}
	jsonValue, _ := json.Marshal(Goals)
	ph.logger.Debug("Goals sends")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonValue))
}

func (ph *GoalHandler) getGoal(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusForbidden)
		return
	}

	goalID, err := strconv.Atoi(mux.Vars(r)["goalID"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	goal, err := ph.GoalUsecase.GetGoal(r.Context(), session.Value, goalID)
	if err != nil {
		ph.logger.Error(err.Error())
		return
	}
	jsonValue, _ := json.Marshal(goal)
	ph.logger.Debug("Goals sends")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonValue))
}


func (ph *GoalHandler) addGoal(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusForbidden)
		return
	}

	mr, err := r.MultipartReader()
	form, err := mr.ReadForm(100000)
	if err != nil {
		ph.logger.Error("no files")
		w.WriteHeader(http.StatusForbidden)
		return
	}
	newGoal := models.Goal{}
	newGoal.Date = form.Value["date"][0]
	newGoal.Title = form.Value["goalTitle"][0]
	newGoal.Description = form.Value["goalDescription"][0]

	err = ph.GoalUsecase.SaveGoal(r.Context(), session.Value, form.File, &newGoal)

	if err != nil {
		ph.logger.Error(err.Error())
		w.WriteHeader(http.StatusForbidden)
		return
	}

	w.WriteHeader(http.StatusOK)
}