package main

//Запускать go run в этой директории, т.к. go run делает бинарник, а пути у нас до staticfileHandlera захардкожены

import (
	"github.com/gorilla/mux"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"my-motivation/internal/app/middleware"
	"my-motivation/internal/app/models"
	user_service_client "my-motivation/internal/pkg/client/user-service-client"
	"my-motivation/internal/pkg/utils/logger"

	_goalHttpDelivery "my-motivation/internal/app/goal/delivery/http"
	_goalRepo "my-motivation/internal/app/goal/repository/psql"
	_goalUsecase "my-motivation/internal/app/goal/usecase"
	_userHttpDelivery "my-motivation/internal/app/user/delivery/http"
	_userRepoPsql "my-motivation/internal/app/user/repository/psql"
	_userUsecase "my-motivation/internal/app/user/usecase"
	//_userRepo "my-motivation/internal/app/user/repository"
	//_postRepo "my-motivation/internal/app/goal/repository"
	_sessionManager "my-motivation/internal/app/session/psql"
	"net/http"
	"os"
	"time"
)


func getPostgres() *gorm.DB {
	dsn := "host=localhost user=MM password=MM dbname=MM port=5400 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	db.AutoMigrate(&models.User{}, &models.Goal{}, &models.Session{}, models.Img{})
	return db
}


func main() {
	//logger
	log := logger.NewLogger()

	//repo
	userRepo := _userRepoPsql.NewUserRepoPsql(getPostgres())
	goalRepo := _goalRepo.NewGoalRepoPsql(getPostgres())
	//friendRepo := _friendRepo.NewFriendRepoPsql(getPostgres())
	sessionManager := _sessionManager.NewSessionsManagerPsql(getPostgres())
	//likeRepo := _likeRepoPsql.NewLikeRepoPsql(getPostgres())
	//albumRepo := _albumRepo.NewAlbumRepoPsql(getPostgres())

	//services
	us, _ := user_service_client.New()

	//usecase
	timeoutContext := 2 * time.Second
	userUsecase := _userUsecase.NewUserUsecase(us, userRepo, timeoutContext, sessionManager)
	goalUsecase := _goalUsecase.NewGoalUsecase(userRepo, goalRepo, timeoutContext, sessionManager)

	//delivery
	r := mux.NewRouter()
	_userHttpDelivery.NewUserHandler(r, userUsecase, log)
	_goalHttpDelivery.NewGoalHandler(r, goalUsecase, log)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	//index
	r.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("Kumusta Higala"))
	})

	//middleware
	handler := middleware.CORS(r)
	handler = middleware.LoggingMiddleware(handler, log)

	server := http.Server{
		Addr:    ":" + os.Getenv("PORT"),
		Handler: handler,
	}

	log.Log.Infof("Server start at %s port", server.Addr)
	server.ListenAndServe()
}
